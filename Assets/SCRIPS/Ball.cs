﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public Vector2 speed;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);

    }
    private void OnTriggerEnter(Collider other)
    {
        switch(other.tag)
        {
            case "RIGHT":
            case "LEFT":
                speed.x = -speed.x;
                break;
            case "UP":
            case "DOWN":
                speed.y = -speed.y;
                break;
            case "Brick":
                other.gameObject.GetComponent<Brick>().Touch();
                speed.y = -speed.y;
                break;



        }
    }
}

