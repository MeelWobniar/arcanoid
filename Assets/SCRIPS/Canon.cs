﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour
{

    public GameObject bullet;
    public float carencia;
    private bool canShoot;
    private float elapsedTime;

    public Bullet[] bullets;
    public int numBullets;
    public Transform posIni;

    private int actualBullet;

    void Awake()
    {
        canShoot = true;
        elapsedTime = 0;
        actualBullet = 0;
        bullets = new Bullet[numBullets];

        for(int i = 0; i < numBullets; i++)
        {
            bullets[i] = Instantiate(bullet, posIni.position, Quaternion.identity, null).GetComponent<Bullet>();
            posIni.Translate(-0.5f, 0, 0);
        }
    }

    private void Update()
    {
        if(!canShoot)
        {
            elapsedTime += Time.deltaTime;
            if(elapsedTime > carencia)
            {
                canShoot = true;
            }
        }
    }

    public void Shoot()
    {
        if(canShoot)
        {
            canShoot = false;
            elapsedTime = 0;

            bullets[actualBullet].Shoot(transform.position);
            actualBullet++;
            if(actualBullet >= numBullets)
            {
                actualBullet = 0;
            }
        }
    }
}