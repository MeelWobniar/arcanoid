﻿+﻿using System.Collections;
+using System.Collections.Generic;
+using UnityEngine;
+
Add a comment to this line
+public class Propeller : MonoBehaviour
{
+
+	public Sprite spriteNormal;
+	public Sprite spriteAccel;
+	private SpriteRenderer sr;
+
+
+	// Use this for initialization
+	void Awake()
    {
        +sr = GetComponent<SpriteRenderer>();
        +Stop();
        +   }
+	
+	public void Blue()
    {
        +sr.enabled = true;
        +sr.sprite = spriteNormal;
        +   }
+
+	public void Red()
    {
        +sr.enabled = true;
        +sr.sprite = spriteAccel;
        +   }
+
+	public void Stop()
    {
        +sr.enabled = false;
        +   }
+}