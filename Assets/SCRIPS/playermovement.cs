﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement : MonoBehaviour {

    public float velocity; //variable de velocidad
    private float axis; // eje
    public float limitx;
   


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        axis = Input.GetAxis("Horizontal"); // movimiento horizaontal 
        transform.Translate(velocity * axis * Time.deltaTime, 0, 0); // propiedades del movimiento en X, Y, Z

        if (transform.position.x > limitx)
        {
            transform.position = new Vector3(limitx, transform.position.y, transform.position.z);
        }

        else if(transform.position.x < -limitx)
        {
            transform.position = new Vector3(-limitx, transform.position.y, transform.position.z);
        }
    }
}
