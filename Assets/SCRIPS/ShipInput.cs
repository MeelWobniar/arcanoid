﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{

    private Vector2 axis;

    public PlayerBehaviour player;
    public Canon canon;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        player.SetAxis(axis);
        if (Input.GetButton("Jump"))
        {
            canon.Shoot();

        }
    }
}
