﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public int lifes;
    public Material green;
    public Material blue;
    public Material yellow;


    // Use this for initialization
    void Awake()
    {
        ChangeColor();
    }

    private void ChangeColor()
    {
        if(lifes == 3)
        {
            //color rojo
            GetComponent<Renderer>().material = green;
        }
        else if(lifes == 2)
        {
            //color azul
            GetComponent<Renderer>().material = blue;
        }
        else
        {
            //color verde
            GetComponent<Renderer>().material = yellow;
        }
    }

    // Update is called once per frame
    public void Touch()
    {
        lifes--;
        if(lifes <= 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            ChangeColor();
        }
    }
}