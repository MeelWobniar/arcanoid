﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed;
    private bool shooting;
    private Vector3 posIni;

    private void Start()
    {
        posIni = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(shooting)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }
    }

    public void Shoot(Vector3 location)
    {
        transform.position = location;
        shooting = true;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Finish")
        {
            shooting = false;
            transform.position = posIni;
        }
    }
}
